# Desafio Devops - Player 2

Olá, obrigado por participar de nosso processo seletivo, sabemos que é bastante difícil encontrar tempo, principalmente quando se está trabalhando, por isso, nosso muito obrigado pelo seu tempo e dedicação.

O código produzido deve estar versionado em algum repositório público (Github, Bitbucket etc.).

Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio Devops - <%SEU_NOME%> " e o link para o seu repositório.

## Conhecimentos necessários para a execução do desafio

- [ ] AWS(Preferencial) ou semelhantes
- [ ] NodeJS
- [ ] ReactJS
- [ ] Git
- [ ] CI/CD
- [ ] SSL
- [ ] Monitoramento da Aplicação
- [ ] Linux

Desejável:
- [ ] Docker
- [ ] Kubernetes

## Por onde começar
Esse desafio tem como intuito que você mostre seus conhecimentos em realizar o deploy de aplicações em NodeJs e ReactJS, passando pelo processo de build.

Não iremos avaliar código em NodeJS ou React, essas aplicações podem ser "Hello World".

## Estrutura da Aplicação

  Ao final, a aplicação terá os seguintes domínios:
  - [ ] API: api-seunome.player2.tech
  - [ ] API de Teste: api-test-seunome.player2.tech
  - [ ] SITE: www-seunome.player2.tech
  - [ ] SITE de Teste: www-test-seunome.player2.tech

Todos esses devem rodar na mesma máquina.
Sinta-se a vontade para sugerir uma abordagem diferente a respeito dos domínios de Teste e Produção.
Nós da Player 2 iremos fornecer os domínios.
Quando você tiver o IP da máquina nos informe para apontarmos os DNS para lá.

## Etapa 1
Nessa etapa esperamos que um servidor EC2 na AWS seja instalado e configurado.

Os Requisitos dessa etapa são:
- [ ] Levantar uma máquina EC2 na AWS
- [ ] Instalar e configurar um servidor Web (Nginx, Apache...)
- [ ] Essa máquina deve rodar os serviços NodeJS e React(Página Estática)
- [ ] Cada serviço deve apontar para um dos subdomínios
- [ ] Cada subdomínio deve ter seu certificado SSL gratuito instalado

## Etapa 2
Nessa etapa esperamos que um CI/CD seja implementado para realizar o deploy automático.

Os Requisitos são:
- [ ] Cada serviço (NodeJS ou React) deve ter seu próprio repositório
- [ ] Devem haver branches para teste e produção
- [ ] Regras de pull/merge request que disparem gatilhos do deploy
- [ ] Opção de realizar um rollback da aplicação para uma versão estável

## Etapa 3
Nessa etapa esperamos que as aplicações sejam monitoradas remotamente.

Os Requisitos são:
- [ ] Monitorar as aplicações
- [ ] Bônus: Alertas de eventos com métricas pensadas por você


## Final
Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio Devops - <%SEU_NOME%> " e o link para o seu repositório e agendarmos uma call para você apresentar e demonstrar.

## Dúvidas
Mande um whatsapp para:

(11) 9-3379-6168 - Fábio

(71) 9-9979-6332 - André Batista

(71) 9-9637-0695 - Iure Silva
